#!/bin/sh

git switch production
git merge default

npm run build

git add .
git commit -m 'Update the production build'

git switch default
