pub enum Interrupt {
    FrameSync = -1,
    TouchInput = -2,
}

// First 2 names aren't official.

pub const WALIGN: u32 = 4;
pub const HALIGN: u32 = 2;
pub const IALIGN: u32 = WALIGN; // See Chapter 1.5 of the unprivileged spec.

// See Chapter 24 of the unprivileged spec.

pub const LOAD: u32 = 0b00_000;
pub const MISC_MEM: u32 = 0b00_011;
pub const OP_IMM: u32 = 0b00_100;
pub const AUIPC: u32 = 0b00_101;
pub const STORE: u32 = 0b01_000;
pub const AMO: u32 = 0b01_011;
pub const OP: u32 = 0b01_100;
pub const LUI: u32 = 0b01_101;
pub const BRANCH: u32 = 0b11_000;
pub const JALR: u32 = 0b11_001;
pub const JAL: u32 = 0b11_011;
pub const SYSTEM: u32 = 0b11_100;

pub const LB: u32 = 0b000;
pub const LH: u32 = 0b001;
pub const LW: u32 = 0b010;
pub const LBU: u32 = 0b100;
pub const LHU: u32 = 0b101;

pub const SB: u32 = 0b000;
pub const SH: u32 = 0b001;
pub const SW: u32 = 0b010;

// Immediate instructions conveniently use the same values for funct3.

pub const ADD: u32 = 0b000;
pub const OR: u32 = 0b110;
pub const AND: u32 = 0b111;
pub const XOR: u32 = 0b100;

pub const SLL: u32 = 0b001;
pub const SLT: u32 = 0b010;
pub const SLTU: u32 = 0b011;
pub const SRL: u32 = 0b101;

pub const BEQ: u32 = 0b000;
pub const BNE: u32 = 0b001;
pub const BLT: u32 = 0b100;
pub const BGE: u32 = 0b101;
pub const BLTU: u32 = 0b110;
pub const BGEU: u32 = 0b111;

pub const MUL: u32 = 0b000;
pub const MULH: u32 = 0b001;
pub const MULHSU: u32 = 0b010;
pub const MULHU: u32 = 0b011;
pub const DIV: u32 = 0b100;
pub const DIVU: u32 = 0b101;
pub const REM: u32 = 0b110;
pub const REMU: u32 = 0b111;

pub const FENCE: u32 = 0b000;
pub const FENCE_I: u32 = 0b001;

pub const PRIV: u32 = 0b000;
pub const CSRRW: u32 = 0b001;
pub const CSRRS: u32 = 0b010;
pub const CSRRC: u32 = 0b011;
pub const CSRRWI: u32 = 0b101;
pub const CSRRSI: u32 = 0b110;
pub const CSRRCI: u32 = 0b111;

pub const LR_W: u32 = 0b00010;
pub const SC_W: u32 = 0b00011;
pub const AMOSWAP_W: u32 = 0b00001;
pub const AMOADD_W: u32 = 0b00000;
pub const AMOXOR_W: u32 = 0b00100;
pub const AMOAND_W: u32 = 0b01100;
pub const AMOOR_W: u32 = 0b01000;
pub const AMOMIN_W: u32 = 0b10000;
pub const AMOMAX_W: u32 = 0b10100;
pub const AMOMINU_W: u32 = 0b11000;
pub const AMOMAXU_W: u32 = 0b11100;

pub const SUBSRA: u32 = 0b0100000; // Not an official name.
pub const MULDIV: u32 = 0b0000001;

pub const ECALL: u32 = 0b000000000000;
pub const EBREAK: u32 = 0b000000000001;

pub const MRET: u32 = 0b0011000_00010;
pub const WFI: u32 = 0b0001000_00101;

// See Table 2.2 of the privileged spec.

pub const CYCLE: u32 = 0xc00;
pub const TIME: u32 = 0xc01;
pub const INSTRET: u32 = 0xc02;

pub const HPMCOUNTER3: u32 = 0xc03;
pub const HPMCOUNTER31: u32 = 0xc1f;

pub const CYCLEH: u32 = 0xc80;
pub const TIMEH: u32 = 0xc81;
pub const INSTRETH: u32 = 0xc82;

pub const HPMCOUNTER3H: u32 = 0xc83;
pub const HPMCOUNTER31H: u32 = 0xc9f;

// See Table 2.5 of the privileged spec.

pub const MVENDORID: u32 = 0xf11;
pub const MARCHID: u32 = 0xf12;
pub const MIMPID: u32 = 0xf13;
pub const MHARTID: u32 = 0xf14;
pub const MCONFIGPTR: u32 = 0xf15;

pub const MSTATUS: u32 = 0x300;
pub const MISA: u32 = 0x301;
pub const MIE: u32 = 0x304;
pub const MTVEC: u32 = 0x305;
pub const MSTATUSH: u32 = 0x310;

pub const MSCRATCH: u32 = 0x340;
pub const MEPC: u32 = 0x341;
pub const MCAUSE: u32 = 0x342;
pub const MTVAL: u32 = 0x343;
pub const MIP: u32 = 0x344;

pub const PMPCFG0: u32 = 0x3a0;
pub const PMPADDR63: u32 = 0x3ef;

// See Table 2.6 of the privileged spec.

pub const MCYCLE: u32 = 0xb00;
pub const MINSTRET: u32 = 0xb02;

pub const MHPMCOUNTER3: u32 = 0xb03;
pub const MHPMCOUNTER31: u32 = 0xb1f;

pub const MCYCLEH: u32 = 0xb80;
pub const MINSTRETH: u32 = 0xb82;

pub const MHPMCOUNTER3H: u32 = 0xb83;
pub const MHPMCOUNTER31H: u32 = 0xb9f;

pub const MHPMEVENT3: u32 = 0x323;
pub const MHPMEVENT31: u32 = 0x33f;

// rv32mi-p-breakpoint expects these to at least exist. I guess this means they
// should.

pub const TSELECT: u32 = 0x7a0;
pub const TDATA3: u32 = 0x7a3;
pub const MCONTEXT: u32 = 0x7a8;

// See Chapter 3.1.7 of the privileged spec.

pub const DIRECT: u32 = 0;
pub const VECTORED: u32 = 1;

// See Figure 3.7 of the privileged spec.

pub const MSTATUS_MIE: u32 = 3;
pub const MSTATUS_MPIE: u32 = 7;
pub const MSTATUS_MPP: u32 = 11;
