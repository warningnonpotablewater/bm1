use std::mem::size_of;

const KB: usize = 1024;
const MB: usize = 1024 * KB;

pub const SRAM_SIZE: usize = 8 * MB;
pub const VRAM_SIZE: usize = 240 * 400; // 1 byte per pixel.
pub const TOUCH_SIZE: usize = 3 * size_of::<u16>();
pub const SOUND_SIZE: usize = 30720 / 30; // Enough audio for 1 video frame.

const fn end_offset(start: usize, size: usize) -> usize {
    return start + size - 1;
}

pub const SRAM_START: usize = 0x80_000000;
pub const SRAM_END: usize = end_offset(SRAM_START, SRAM_SIZE);

pub const VRAM_START: usize = 0x01_000000;
pub const VRAM_END: usize = end_offset(VRAM_START, VRAM_SIZE);

pub const TOUCH_START: usize = 0x02_000000;
pub const TOUCH_END: usize = end_offset(TOUCH_START, TOUCH_SIZE);

pub const SOUND_START: usize = 0x03_000000;
pub const SOUND_END: usize = end_offset(SOUND_START, SOUND_SIZE);
