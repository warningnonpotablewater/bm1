use std::cmp::{min, max};

use crate::{bit_logic::*, constants::*};
use crate::{bus::Bus, memory_map::SRAM_START};

// Irrelevant exceptions aren't implemented.

#[derive(Copy, Clone)]
pub enum Exception {
    InstructionAddressMisaligned = 0,
    InstructionAccessFault = 1,
    IllegalInstruction = 2,
    Breakpoint = 3,
    LoadAddressMisaligned = 4,
    LoadAccessFault = 5,
    StoreAddressMisaligned = 6,
    StoreAccessFault = 7,
    Ecall = 11,
}

pub struct Cpu {
    pub bus: Bus,

    pub pc: u32,
    pub x: [u32; 32],

    pub mstatus: Mstatus,
    pub mtvec: u32,
    pub mcycle: u64,
    pub minstret: u64,
    pub mscratch: u32,
    pub mepc: u32,
    pub mcause: u32,

    pub amo_reservation: Option<u32>,
    pub waiting_for_interrupt: bool,
}

#[derive(Copy, Clone)]
pub struct Mstatus {
    pub mie: bool,
    pub mpie: bool,
}

impl Cpu {
    pub fn new(bus: Bus) -> Self {
        return Self {
            bus,

            pc: SRAM_START as u32,
            x: [0; 32],

            mstatus: Mstatus {
                mie: false,
                mpie: false,
            },
            mtvec: 0,
            mcycle: 0,
            minstret: 0,
            mscratch: 0,
            mepc: 0,
            mcause: 0,

            amo_reservation: None,
            waiting_for_interrupt: false,
        };
    }

    pub fn cycle(&mut self) -> Result<(), u32> {
        #[cfg(feature = "debug")]
        println!("@{:x}", self.pc);

        // Compressed instructions aren't implemented, so we don't check for
        // those.

        if self.pc % IALIGN != 0 {
            return self.except(Exception::InstructionAddressMisaligned);
        }

        let instruction = match self.bus.read_u32(self.pc) {
            Ok(word) => word,
            Err(_) => return self.except(Exception::InstructionAccessFault),
        };

        // See Chapter 1.5 of the unprivileged spec.

        let empty = slice_bits(instruction, 0, 15) == 0;
        let short = slice_bits(instruction, 0, 1) != 0b11;
        let long = slice_bits(instruction, 2, 4) == 0b111;

        if empty || short || long {
            return self.except(Exception::IllegalInstruction);
        }

        // The lowest 2 bits are always the same, so there's no need to check
        // them.

        let opcode = slice_bits(instruction, 2, 6);

        let mut new_pc = self.pc + 4;

        match opcode {
            LOAD => {
                let dest = decode_rd(instruction);
                let width = decode_funct3(instruction);
                let base = decode_rs1(instruction);
                let offset = decode_i_imm(instruction);

                let address = self.read_register(base) + offset;

                let misaligned = match width {
                    LH | LHU => address % HALIGN != 0,
                    LW => address % WALIGN != 0,
                    _ => false,
                };

                if misaligned {
                    return self.except(Exception::LoadAddressMisaligned);
                }

                let result = match width {
                    LB => self.bus.read_u8(address),
                    LH => self.bus.read_u16(address),
                    LW => self.bus.read_u32(address),
                    LBU => self.bus.read_u8(address),
                    LHU => self.bus.read_u16(address),
                    _ => return self.except(Exception::IllegalInstruction),
                };

                let value = match result {
                    Ok(int) => match width {
                        LB => sign_extend(7, int),
                        LH => sign_extend(15, int),
                        _ => int,
                    },
                    Err(_) => return self.except(Exception::LoadAccessFault),
                };

                self.write_register(dest, value);
            },
            MISC_MEM => {
                // Everything is executed in-order and there's only one core, so
                // there's no point in actually synchronizing anything. As such,
                // FENCE and FENCE.I are stubbed.

                match decode_funct3(instruction) {
                    FENCE => (),
                    FENCE_I => (),
                    _ => return self.except(Exception::IllegalInstruction),
                }
            }
            OP_IMM => {
                let dest = decode_rd(instruction);
                let operation = decode_funct3(instruction);
                let src = decode_rs1(instruction);

                let a = self.read_register(src);
                let b = decode_i_imm(instruction);

                // Needed for signed operations.

                let a_signed = a as i32;
                let b_signed = b as i32;

                // Technically, shift operations also use I-type instructions,
                // but the immediate happens to be split at the same offsets.

                let shamt = decode_rs2(instruction);
                let shift_type = decode_funct7(instruction);

                let result = match operation {
                    ADD => a + b,
                    AND => a & b,
                    OR => a | b,
                    XOR => a ^ b,
                    SLT => (a_signed < b_signed) as u32,
                    SLTU => (a < b) as u32,
                    SLL => match shift_type {
                        0 => a << shamt,
                        _ => return self.except(Exception::IllegalInstruction),
                    },
                    SRL => match shift_type {
                        0 => a >> shamt,
                        SUBSRA => (a_signed >> shamt) as u32,
                        _ => return self.except(Exception::IllegalInstruction),
                    }
                    _ => return self.except(Exception::IllegalInstruction),
                };

                self.write_register(dest, result);
            },
            AUIPC => {
                let dest = decode_rd(instruction);
                let value = decode_u_imm(instruction);

                self.write_register(dest, self.pc + value);
            }
            STORE => {
                let width = decode_funct3(instruction);
                let base = decode_rs1(instruction);
                let src = decode_rs2(instruction);
                let offset = decode_s_imm(instruction);

                let address = self.read_register(base) + offset;
                let value = self.read_register(src);

                let misaligned = match width {
                    SH => address % HALIGN != 0,
                    SW => address % WALIGN != 0,
                    _ => false,
                };

                if misaligned {
                    return self.except(Exception::StoreAddressMisaligned);
                }

                let result = match width {
                    SB => self.bus.write_u8(address, value),
                    SH => self.bus.write_u16(address, value),
                    SW => self.bus.write_u32(address, value),
                    _ => return self.except(Exception::IllegalInstruction),
                };

                match result {
                    Ok(_) => (),
                    Err(_) => return self.except(Exception::StoreAccessFault),
                };
            },
            AMO => {
                let dest = decode_rd(instruction);
                let width = decode_funct3(instruction);
                let addr = decode_rs1(instruction);
                let src = decode_rs2(instruction);
                let funct5 = slice_bits(decode_funct7(instruction), 2, 6);
                // aq and rl fields are ignored.

                if width != 0b010 {
                    return self.except(Exception::IllegalInstruction);
                }

                // src and dest can be the same, so it has to be read here.

                let value = self.read_register(src);

                let mut load_address: Option<u32> = None;
                let mut store_address: Option<u32> = None;

                match funct5 {
                    LR_W => load_address = Some(self.read_register(addr)),
                    SC_W => store_address = Some(self.read_register(addr)),
                    AMOSWAP_W | AMOADD_W | AMOXOR_W | AMOAND_W | AMOOR_W
                    | AMOMIN_W | AMOMAX_W | AMOMINU_W | AMOMAXU_W => {
                        load_address = Some(self.read_register(addr));
                        store_address = load_address;
                    },
                    _ => return self.except(Exception::IllegalInstruction),
                }

                let input = match load_address {
                    Some(address) => {
                        if address % WALIGN != 0 {
                            return self.except(
                                Exception::LoadAddressMisaligned
                            );
                        }

                        let result = self.bus.read_u32(address);

                        match result {
                            Ok(int) => int,
                            Err(_) =>
                                return self.except(Exception::LoadAccessFault),
                        }
                    },
                    None => 0,
                };

                self.write_register(dest, input);

                match funct5 {
                    LR_W => self.amo_reservation = Some(addr),
                    SC_W => match self.amo_reservation {
                        Some(address) => if address != addr {
                            self.cancel_amo_reservation(dest);

                            store_address = None;
                        },
                        None => {
                            self.cancel_amo_reservation(dest);

                            store_address = None;
                        },
                    },
                    _ => (),
                }

                // Needed for signed comparisons.

                let signed_value = value as i32;
                let signed_input = input as i32;

                let output = match funct5 {
                    LR_W => 0,
                    SC_W | AMOSWAP_W => value,
                    AMOADD_W => input + value,
                    AMOXOR_W => input ^ value,
                    AMOAND_W => input & value,
                    AMOOR_W => input | value,
                    AMOMIN_W => min(signed_input, signed_value) as u32,
                    AMOMAX_W => max(signed_input, signed_value) as u32,
                    AMOMINU_W => min(input, value),
                    AMOMAXU_W => max(input, value),
                    _ => return self.except(Exception::IllegalInstruction),
                };

                match store_address {
                    Some(address) => {
                        if address % WALIGN != 0 {
                            return self.except(
                                Exception::StoreAddressMisaligned
                            );
                        }

                        let result = self.bus.write_u32(address, output);

                        match result {
                            Ok(_) => if funct5 == SC_W {
                                self.amo_reservation = None;
                            },
                            Err(_) => {
                                if funct5 == SC_W {
                                    self.cancel_amo_reservation(dest);
                                }

                                return self.except(Exception::StoreAccessFault);
                            }
                        }
                    },
                    None => (),
                }
            },
            OP => {
                let dest = decode_rd(instruction);
                let operation = decode_funct3(instruction);
                let src1 = decode_rs1(instruction);
                let src2 = decode_rs2(instruction);

                let a = self.read_register(src1);
                let b = self.read_register(src2);

                // Only the lowest 5 bits count in the shift amount. See Chapter
                // 2.4 of the unprivileged spec.

                let shamt = slice_bits(b, 0, 4);

                // Needed for signed operations.

                let a_signed = a as i32;
                let b_signed = b as i32;

                // Needed to get the high bits of multiplication results.

                let a_wide = a as i64;
                let a_wide_signed = a_signed as i64;
                let b_wide = b as i64;
                let b_wide_signed = b_signed as i64;

                let by_zero = b == 0;
                let overflowing = a_signed == i32::MIN && b_signed == -1;

                let result = match decode_funct7(instruction) {
                    0 => match operation {
                        ADD => a + b,
                        AND => a & b,
                        OR => a | b,
                        XOR => a ^ b,
                        SLT => (a_signed < b_signed) as u32,
                        SLTU => (a < b) as u32,
                        SLL => a << shamt,
                        SRL => a >> shamt,
                        _ => return self.except(Exception::IllegalInstruction),
                    },
                    SUBSRA => match operation {
                        ADD => a - b,
                        SRL => (a_signed >> shamt) as u32,
                        _ => return self.except(Exception::IllegalInstruction),
                    },
                    MULDIV => match operation {
                        MUL => a * b,
                        MULH => i64_high(a_wide_signed * b_wide_signed),
                        MULHSU => i64_high(a_wide_signed * b_wide),
                        MULHU => i64_high(a_wide * b_wide),

                        // See Chapter 7.2 of the unprivileged spec.

                        DIV =>
                            if by_zero {u32::MAX}
                            else if overflowing {i32::MIN as u32}
                            else {(a_signed / b_signed) as u32},
                        DIVU => if by_zero {u32::MAX} else {a / b},
                        REM =>
                            if by_zero {a}
                            else if overflowing {0}
                            else {(a_signed % b_signed) as u32},
                        REMU => if by_zero {a} else {a % b},
                        _ => return self.except(Exception::IllegalInstruction),
                    },
                    _ => return self.except(Exception::IllegalInstruction),
                };

                self.write_register(dest, result);
            },
            LUI => {
                let dest = decode_rd(instruction);
                let value = decode_u_imm(instruction);

                self.write_register(dest, value);
            },
            BRANCH => {
                let operation = decode_funct3(instruction);
                let src1 = decode_rs1(instruction);
                let src2 = decode_rs2(instruction);
                let offset = decode_b_imm(instruction);

                let a = self.read_register(src1);
                let b = self.read_register(src2);

                let success = match operation {
                    BEQ => a == b,
                    BNE => a != b,
                    BLT => (a as i32) < (b as i32),
                    BGE => (a as i32) >= (b as i32),
                    BLTU => a < b,
                    BGEU => a >= b,
                    _ => return self.except(Exception::IllegalInstruction),
                };

                if success {
                    new_pc = self.pc + offset;
                }

                if new_pc % IALIGN != 0 {
                    return self.except(Exception::InstructionAddressMisaligned);
                }
            },
            JALR => {
                if decode_funct3(instruction) != 0 {
                    return self.except(Exception::IllegalInstruction);
                }

                let dest = decode_rd(instruction);
                let base = decode_rs1(instruction);
                let offset = decode_i_imm(instruction);

                let address = self.read_register(base) + offset;

                // The lowest bit has to be set to 0. See Chapter 2.5 of the
                // unprivileged spec.

                new_pc = clear_bits(address, 0, 0);

                if new_pc % IALIGN != 0 {
                    return self.except(Exception::InstructionAddressMisaligned);
                }

                self.write_register(dest, self.pc + 4);
            },
            JAL => {
                let dest = decode_rd(instruction);
                let offset = decode_j_imm(instruction);

                new_pc = self.pc + offset;

                if new_pc % IALIGN != 0 {
                    return self.except(Exception::InstructionAddressMisaligned);
                }

                self.write_register(dest, self.pc + 4);
            },
            SYSTEM => {
                let dest = decode_rd(instruction);
                let operation = decode_funct3(instruction);
                let source = decode_rs1(instruction);
                let csr = decode_csr_imm(instruction);

                let funct12 = decode_i_imm(instruction);
                let uimm = source;

                match operation {
                    PRIV => match funct12 {
                        ECALL => return self.except(Exception::Ecall),
                        EBREAK => return self.except(Exception::Breakpoint),
                        MRET => {
                            self.mstatus.mie = self.mstatus.mpie;
                            self.mstatus.mpie = true;

                            new_pc = self.mepc;
                        },
                        WFI => self.waiting_for_interrupt = true,
                        _ => return self.except(Exception::IllegalInstruction),
                    },
                    CSRRW => {
                        // Source can be the same as dest, so it's read first.

                        let value = self.read_register(source);

                        self.read_csr_to_register(csr, dest)?;
                        self.write_csr(csr, value)?;
                    },
                    CSRRS => {
                        let mask = self.read_register(source);
                        let value = self.read_csr_to_register(csr, dest)?;

                        if source != 0 {
                            self.write_csr(csr, value | mask)?;
                        }
                    },
                    CSRRC => {
                        let mask = self.read_register(source);
                        let value = self.read_csr_to_register(csr, dest)?;

                        if source != 0 {
                            self.write_csr(csr, value & !mask)?;
                        }
                    },
                    CSRRWI => {
                        self.read_csr_to_register(csr, dest)?;
                        self.write_csr(csr, uimm)?;
                    },
                    CSRRSI => {
                        let value = self.read_csr_to_register(csr, dest)?;

                        if uimm != 0 {
                            self.write_csr(csr, value | uimm)?;
                        }
                    },
                    CSRRCI => {
                        let value = self.read_csr_to_register(csr, dest)?;

                        if uimm != 0 {
                            self.write_csr(csr, value & !uimm)?;
                        }
                    },
                    _ => return self.except(Exception::IllegalInstruction),
                }
            },
            _ => return self.except(Exception::IllegalInstruction),
        }

        self.pc = new_pc;

        self.mcycle += 1;
        self.minstret += 1;

        return Ok(());
    }

    fn except(&mut self, code: Exception) -> Result<(), u32> {
        let cause = code as u32;

        #[cfg(feature = "debug")]
        println!("Exception {} at address {:x}", cause, self.pc);

        self.trap(cause);

        // Exceptions can't be vectored. See Chapter 3.1.7 of the privileged
        // spec.

        self.pc = clear_bits(self.mtvec, 0, 1);

        // minstret shouldn't be incremented if there's an exception, so it's
        // done only after successfully executing an instruction. See Chapter
        // 3.3.1 of the privileged spec.

        return Err(cause);
    }

    pub fn interrupt(&mut self, code: Interrupt) -> Result<(), ()> {
        let cause = code as u32;

        if !self.mstatus.mie {
            return Err(());
        }

        self.trap(cause);

        let trap_mode = slice_bits(self.mtvec, 0, 1);
        let base = clear_bits(self.mtvec, 0, 1);
        let vector = slice_bits(cause, 0, 30);

        self.pc = match trap_mode {
            DIRECT => base,
            VECTORED => base + vector * 4,
            _ => base,
        };

        return Ok(());
    }

    fn trap(&mut self, cause: u32) {
        self.mstatus.mpie = self.mstatus.mie;
        self.mstatus.mie = false;

        self.mepc = self.pc;
        self.mcause = cause;

        self.mcycle += 1;

        self.waiting_for_interrupt = false;
    }

    fn read_register(&mut self, index: u32) -> u32 {
        if index == 0 || index > 31 {
            return 0;
        }

        return self.x[index as usize];
    }

    fn write_register(&mut self, index: u32, value: u32) {
        if index == 0 || index > 31 {
            return;
        }

        self.x[index as usize] = value;
    }

    fn cancel_amo_reservation(&mut self, dest: u32) {
        self.amo_reservation = None;
        self.write_register(dest, 1);
    }

    fn read_csr_to_register(
        &mut self,
        csr: u32,
        dest: u32
    ) -> Result<u32, u32> {
        let value = match csr {
            MSTATUS => self.mstatus.into(),
            MTVEC => self.mtvec,
            MSCRATCH => self.mscratch,
            MEPC => self.mepc,
            MCAUSE => self.mcause,

            CYCLE | MCYCLE => self.mcycle as u32,
            CYCLEH | MCYCLEH => u64_high(self.mcycle),

            INSTRET | MINSTRET => self.minstret as u32,
            INSTRETH | MINSTRETH => u64_high(self.minstret),

            // The spec doesn't mandate any tick speed, so it's running at 0 Hz.

            TIME => 0,
            TIMEH => 0,

            // Guaranteed to always be read-only.

            MVENDORID => 0, // "This is a non-commercial implementation."
            MARCHID => 0,
            MIMPID => 0xb1a571, // "blast1"
            MHARTID => 0,
            MCONFIGPTR => 0,

            // Optionally allowed to be read-only by the spec.

            //        32      ZYXWVUTSRQPONMLQJIHGFEDCBA
            MISA => 0b01_0000_00000000000001000100000001,
            MIE => 0, // Probably legal™ as far as I understand.
            MSTATUSH => 0, // Only has MBE which is optional.
            MTVAL => 0,
            MIP => 0,
            PMPCFG0..=PMPADDR63 => 0,
            MHPMCOUNTER3..=MHPMCOUNTER31 | HPMCOUNTER3..=HPMCOUNTER31 => 0,
            MHPMCOUNTER3H..=MHPMCOUNTER31H | HPMCOUNTER3H..=HPMCOUNTER31H => 0,
            MHPMEVENT3..=MHPMEVENT31 => 0,
            TSELECT..=TDATA3 | MCONTEXT => 0,

            _ => match self.except(Exception::IllegalInstruction) {
                Ok(_) => unreachable!(),
                Err(cause) => return Err(cause),
            },
        };

        if dest == 0 {
            return Ok(value);
        }

        self.write_register(dest, value);

        return Ok(value);
    }

    fn write_csr(&mut self, index: u32, value: u32) -> Result<(), u32> {
        match index {
            MSTATUS => self.mstatus = value.into(),
            MTVEC => self.mtvec = value,
            MSCRATCH => self.mscratch = value,
            MEPC => self.mepc = value,
            MCAUSE => self.mcause = value,

            MCYCLE => self.mcycle = u64_replace_low(self.mcycle, value),
            MCYCLEH => self.mcycle = u64_replace_high(self.mcycle, value),

            MINSTRET => self.minstret = u64_replace_low(self.minstret, value),
            MINSTRETH => self.minstret = u64_replace_high(self.minstret, value),

            // Read-only.

            CYCLE | CYCLEH | TIME | TIMEH | INSTRET | INSTRETH
            | HPMCOUNTER3..=HPMCOUNTER31 | HPMCOUNTER3H..=HPMCOUNTER31H
            | MVENDORID | MARCHID | MIMPID | MHARTID | MCONFIGPTR
            => return self.except(Exception::IllegalInstruction),

            // Hard-wired to a single value.

            MISA | MIE | MSTATUSH | MTVAL | MIP | PMPCFG0..=PMPADDR63
            | MHPMCOUNTER3..=MHPMCOUNTER31 | MHPMCOUNTER3H..=MHPMCOUNTER31H
            | MHPMEVENT3..=MHPMEVENT31 | TSELECT..=TDATA3 | MCONTEXT => (),

            _ => return self.except(Exception::IllegalInstruction),
        };

        return Ok(());
    }
}

impl From<u32> for Mstatus {
    fn from(int: u32) -> Self {
        return Self {
            mie: slice_bits(int, MSTATUS_MIE, MSTATUS_MIE) != 0,
            mpie: slice_bits(int, MSTATUS_MPIE, MSTATUS_MPIE) != 0,
        };
    }
}

impl Into<u32> for Mstatus {
    fn into(self) -> u32 {
        let mpp: u32 = 0b11; // The privilege level is always M.
        let mie = self.mie as u32;
        let mpie = self.mpie as u32;

        return mpp << MSTATUS_MPP | mie << MSTATUS_MIE | mpie << MSTATUS_MPIE;
    }
}
