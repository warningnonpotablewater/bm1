use std::mem::size_of;

use crate::{memory_map::*, gpu::Gpu, sound::Sound, touch::Touch};

enum Intent {
    Read,
    Write,
}

pub struct Bus {
    pub sram: Vec<u8>,

    pub gpu: Gpu,
    pub touch: Touch,
    pub sound: Sound,
}

impl Bus {
    pub fn new() -> Self {
        return Self {
            sram: vec![0; SRAM_SIZE],

            gpu: Gpu::new(),
            touch: Touch::new(),
            sound: Sound::new(),
        };
    }

    pub fn load_rom(&mut self, rom: &[u8]) -> Result<(), ()> {
        let size = rom.len();

        if size > SRAM_SIZE {
            return Err(());
        }

        self.sram[0..size].copy_from_slice(&rom);

        return Ok(());
    }

    // The caller must check if the address is aligned to the type, or else
    // everything will blow up. This was done in order to simplify memory region
    // calculations - if the most you can load/store at once is 32 bits, then as
    // long as the regions themselves are aligned to 32 bits and loads/stores
    // are aligned to their type, a situaion where 2 regions are accessed at
    // once can't occur.

    #[inline(always)]
    pub fn read_u8(&mut self, address: u32) -> Result<u32, ()> {
        let slice = self.slice_at_address::<u8>(address, Intent::Read)?;

        return Ok(slice[0] as u32);
    }

    #[inline(always)]
    pub fn read_u16(&mut self, address: u32) -> Result<u32, ()> {
        let slice = self.slice_at_address::<u16>(address, Intent::Read)?;
        let result = u16::from_le_bytes(slice.try_into().unwrap());

        return Ok(result as u32);
    }

    #[inline(always)]
    pub fn read_u32(&mut self, address: u32) -> Result<u32, ()> {
        let slice = self.slice_at_address::<u32>(address, Intent::Read)?;
        let result = u32::from_le_bytes(slice.try_into().unwrap());

        return Ok(result);
    }

    #[inline(always)]
    pub fn write_u8(&mut self, address: u32, value: u32) -> Result<(), ()> {
        let slice = self.slice_at_address::<u8>(address, Intent::Write)?;

        slice[0] = value as u8;

        return Ok(());
    }

    #[inline(always)]
    pub fn write_u16(&mut self, address: u32, value: u32) -> Result<(), ()> {
        let slice = self.slice_at_address::<u16>(address, Intent::Write)?;
        let result = (value as u16).to_le_bytes();

        slice.copy_from_slice(&result);

        return Ok(());
    }

    #[inline(always)]
    pub fn write_u32(&mut self, address: u32, value: u32) -> Result<(), ()> {
        let slice = self.slice_at_address::<u32>(address, Intent::Write)?;
        let result = value.to_le_bytes();

        slice.copy_from_slice(&result);

        return Ok(());
    }

    #[inline(always)]
    fn slice_at_address<T>(
        &mut self,
        address: u32,
        intent: Intent,
    ) -> Result<&mut [u8], ()> {
        let size = size_of::<T>();

        let global_offset = address as usize;
        let (region, local_offset) =
            self.translate_offset(global_offset, intent)?;

        let low = local_offset;
        let high = local_offset + size; // Non-inclusive.

        return Ok(&mut region[low..high]);
    }

    #[inline(always)]
    fn translate_offset(
        &mut self,
        offset: usize,
        intent: Intent,
    ) -> Result<(&mut Vec<u8>, usize), ()> {
        return match offset {
            SRAM_START..=SRAM_END => Ok((&mut self.sram, offset - SRAM_START)),
            VRAM_START..=VRAM_END => {
                match intent {
                    Intent::Write => self.gpu.dirty = true,
                    _ => (),
                }

                Ok((&mut self.gpu.vram, offset - VRAM_START))
            },
            TOUCH_START..=TOUCH_END => match intent {
                Intent::Read =>
                    Ok((&mut self.touch.data, offset - TOUCH_START)),
                Intent::Write => Err(()),
            },
            SOUND_START..=SOUND_END => {
                match intent {
                    Intent::Write => self.sound.dirty = true,
                    _ => (),
                }

                Ok((&mut self.sound.data, offset - SOUND_START))
            },
            _ => Err(()),
        };
    }
}
