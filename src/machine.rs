use wasm_bindgen::prelude::*;

use crate::{bus::Bus, constants::Interrupt, cpu::Cpu, touch::Input};

pub const CYCLES_PER_SECOND: u32 = 16_000_020;
pub const FRAMES_PER_SECOND: u32 = 30;
pub const CYCLES_PER_FRAME: u32 = CYCLES_PER_SECOND / FRAMES_PER_SECOND;

#[wasm_bindgen]
pub struct Machine {
    cpu: Cpu,
}

#[wasm_bindgen]
impl Machine {
    #[wasm_bindgen(constructor)]
    pub fn new(rom: &[u8]) -> Self {
        let mut bus = Bus::new();

        match bus.load_rom(&rom) {
            Ok(_) => (),
            Err(_) => unreachable!(),
        }

        return Self {
            cpu: Cpu::new(bus)
        };
    }

    pub fn dump(&self, dest: &mut [u8]) {
        dest.copy_from_slice(&self.cpu.bus.sram);
    }

    pub fn frame(&mut self, framebuffer: &mut [u8], audio_buffer: &mut [f32]) {
        let mut sync_served = false;
        let mut inputs_served = false;

        for _ in 0..CYCLES_PER_FRAME {
            if !sync_served {
                match self.cpu.interrupt(Interrupt::FrameSync) {
                    Ok(_) => sync_served = true,
                    Err(_) => (),
                }
            }

            if !inputs_served {
                match self.cpu.bus.touch.front() {
                    Some(interrupt) => match self.cpu.interrupt(interrupt) {
                        Ok(_) => self.cpu.bus.touch.pop(),
                        Err(_) => (),
                    },
                    None => inputs_served = true,
                };
            }

            if self.cpu.waiting_for_interrupt {
                break;
            }

            let _ = self.cpu.cycle();
        }

        self.cpu.bus.gpu.render(framebuffer);
        self.cpu.bus.sound.render(audio_buffer);
    }

    pub fn enqueue_touch_down(&mut self, x: u16, y: u16) {
        self.cpu.bus.touch.enqueue(Input::Down(x, y));
    }

    pub fn enqueue_touch_move(&mut self, x: u16, y: u16) {
        self.cpu.bus.touch.enqueue(Input::Move(x, y));
    }

    pub fn enqueue_touch_up(&mut self, x: u16, y: u16) {
        self.cpu.bus.touch.enqueue(Input::Up(x, y));
    }
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(message: &str);
}

pub fn debug(message: String) {
    log(message.as_str());
}

#[wasm_bindgen]
pub fn set_panic_hook() {
    // https://github.com/rustwasm/console_error_panic_hook#readme

    console_error_panic_hook::set_once();
}
