use crate::memory_map::SOUND_SIZE;

pub struct Sound {
    pub data: Vec<u8>,
    pub dirty: bool,
}

impl Sound {
    pub fn new() -> Self {
        return Self {
            data: vec![0; SOUND_SIZE],
            dirty: true,
        };
    }

    pub fn render(&mut self, audio_buffer: &mut [f32]) {
        if !self.dirty {
            return;
        }

        for (i, sample) in self.data.iter().enumerate() {
            let signed_sample = *sample as i8;

            audio_buffer[i] = signed_sample as f32 / i8::MAX as f32;
        }

        self.dirty = false;
    }
}
