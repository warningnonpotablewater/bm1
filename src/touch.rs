use std::{collections::VecDeque, mem::size_of};

use crate::{constants::Interrupt, memory_map::TOUCH_SIZE};

const MAX_QUEUE_LENGTH: usize = 6;

pub enum Input {
    Down(u16, u16),
    Move(u16, u16),
    Up(u16, u16),
}

pub struct Touch {
    pub data: Vec<u8>,

    queue: VecDeque<Input>,
}

impl Touch {
    pub fn new() -> Self {
        return Self {
            data: vec![0; TOUCH_SIZE],

            queue: VecDeque::with_capacity(MAX_QUEUE_LENGTH),
        };
    }

    pub fn enqueue(&mut self, input: Input) {
        self.queue.push_back(input);

        if self.queue.len() > MAX_QUEUE_LENGTH {
            self.pop();
        }
    }

    pub fn front(&mut self) -> Option<Interrupt> {
        let input = match self.queue.front() {
            Some(i) => i,
            None => return None,
        };

        match input {
            Input::Down(x, y) => self.set_data(0, *x, *y),
            Input::Move(x, y) => self.set_data(1, *x, *y),
            Input::Up(x, y) => self.set_data(2, *x, *y),
        };

        return Some(Interrupt::TouchInput);
    }

    pub fn pop(&mut self) {
        let _ = self.queue.pop_front();
    }

    #[inline(always)]
    fn set_data(&mut self, input: u16, x: u16, y: u16) {
        self.set_u16(0, input);
        self.set_u16(1, x);
        self.set_u16(2, y);
    }

    #[inline(always)]
    fn set_u16(&mut self, index: usize, value: u16) {
        let low = index * size_of::<u16>();
        let high = (index + 1) * size_of::<u16>(); // Non-inclusive.

        let slice = &mut self.data[low..high];
        let value_as_slice = value.to_le_bytes();

        slice.copy_from_slice(&value_as_slice);
    }
}
