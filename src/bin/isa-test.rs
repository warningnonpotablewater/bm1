use std::env;
use std::fs::read;
use std::process::ExitCode;

use bm1::{bus::Bus, cpu::{Cpu, Exception}};

fn main() -> ExitCode {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("usage: isa-test <case>");

        return ExitCode::FAILURE;
    }

    let filename = &args[1];

    match read(filename) {
        Ok(test_case) => return run(test_case),
        Err(err) => eprintln!("error: {}: {}", filename, err),
    }

    return ExitCode::FAILURE;
}

fn run(test_case: Vec<u8>) -> ExitCode {
    let mut bus = Bus::new();

    match bus.load_rom(&test_case) {
        Ok(_) => (),
        Err(_) => unreachable!(),
    };

    let mut cpu = Cpu::new(bus);

    loop {
        let status = cpu.cycle();

        match status {
            Ok(_) => (),
            Err(cause) => if cause == Exception::Ecall as u32 {
                break;
            },
        }
    }

    // See riscv_test.h from riscv-tests.

    if cpu.x[3] == 1 && cpu.x[10] == 0 {
        return ExitCode::SUCCESS;
    }

    return ExitCode::FAILURE;
}
