use crate::memory_map::VRAM_SIZE;

pub struct Gpu {
    pub vram: Vec<u8>,
    pub dirty: bool,
}

impl Gpu {
    pub fn new() -> Self {
        return Self {
            vram: vec![0; VRAM_SIZE],
            dirty: true,
        };
    }

    pub fn render(&mut self, framebuffer: &mut [u8]) {
        if !self.dirty {
            return;
        }

        for (i, color) in self.vram.iter().enumerate() {
            let red = i * 4;
            let green = red + 1;
            let blue = red + 2;
            let alpha = red + 3;

            framebuffer[red] = *color;
            framebuffer[green] = *color;
            framebuffer[blue] = *color;
            framebuffer[alpha] = 0xff;
        }

        self.dirty = false;
    }
}
