// The [start; end] range is closed.

pub const fn slice_bits(value: u32, start: u32, end: u32) -> u32 {
    let len = end as i32 - start as i32 + 1;

    if len <= 0 {
        return 0;
    }
    if len >= 32 {
        return value;
    }

    let mask = (1 << len) - 1;
    let overlap = (value >> start) & mask;

    return overlap;
}

pub const fn clear_bits(value: u32, start: u32, end: u32) -> u32 {
    let len = end as i32 - start as i32 + 1;

    if len <= 0 {
        return value;
    }
    if len >= 32 {
        return 0;
    }

    let mask = ((1 << len) - 1) << start;
    let overlap = value & !mask;

    return overlap;
}

// Native sign-extension is also synthesized using 2 shifts unless the B
// extension is present. See RISC-V Assembly Programmer's Manual.

pub const fn sign_extend(position: u32, value: u32) -> u32 {
    if position >= 32 {
        return 0;
    }

    let amount = 32 - position - 1;

    return ((value << amount) as i32 >> amount) as u32;
}

pub const fn u64_high(input: u64) -> u32 {
    return (input >> 32) as u32;
}

pub const fn i64_high(input: i64) -> u32 {
    return u64_high(input as u64);
}

pub const fn u64_replace_low(input: u64, value: u32) -> u64 {
    let mask = (u32::MAX as u64) << 32;

    let low = value as u64;
    let high = input & mask;

    return low | high;
}

pub const fn u64_replace_high(input: u64, value: u32) -> u64 {
    let mask = u32::MAX as u64;

    let low = input & mask;
    let high = (value as u64) << 32;

    return low | high;
}

// See Chapter 2.3 of the unprivileged spec.

pub const fn decode_rd(instruction: u32) -> u32 {
    return slice_bits(instruction, 7, 11);
}

pub const fn decode_rs1(instruction: u32) -> u32 {
    return slice_bits(instruction, 15, 19);
}

pub const fn decode_rs2(instruction: u32) -> u32 {
    return slice_bits(instruction, 20, 24);
}

pub const fn decode_funct3(instruction: u32) -> u32 {
    return slice_bits(instruction, 12, 14);
}

pub const fn decode_funct7(instruction: u32) -> u32 {
    return slice_bits(instruction, 25, 31);
}

// I-type immediate but without sign-extension. Needed for the CSR instructions.

pub const fn decode_csr_imm(instruction: u32) -> u32 {
    return slice_bits(instruction, 20, 31);
}

pub const fn decode_i_imm(instruction: u32) -> u32 {
    return sign_extend(11, decode_csr_imm(instruction));
}

pub const fn decode_s_imm(instruction: u32) -> u32 {
    return sign_extend(11,
        slice_bits(instruction, 7, 11)
        | (slice_bits(instruction, 25, 31) << 5)
    );
}

pub const fn decode_b_imm(instruction: u32) -> u32 {
    return sign_extend(12,
        (slice_bits(instruction, 7, 7) << 11)
        | (slice_bits(instruction, 8, 11) << 1)
        | (slice_bits(instruction, 25, 30) << 5)
        | (slice_bits(instruction, 31, 31) << 12)
    );
}

pub const fn decode_u_imm(instruction: u32) -> u32 {
    // The highest bit already happens to be where it should be, so there's no
    // need to sign-extend.

    return slice_bits(instruction, 12, 31) << 12;
}

pub const fn decode_j_imm(instruction: u32) -> u32 {
    return sign_extend(20,
        (slice_bits(instruction, 12, 19) << 12)
        | (slice_bits(instruction, 20, 20) << 11)
        | (slice_bits(instruction, 21, 30) << 1)
        | (slice_bits(instruction, 31, 31) << 20)
    );
}
