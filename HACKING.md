# BlastMachine One hardware reference

## CPU

The machine provides a RISC-V core with the following properties:

* Single hart
* In-order execution
* Little-endian memory with no misaligned load/store support
* Clock speed of 16 MHz with 1 instruction executed per each cycle

The following specifications are fully implemented:

* RV32I base integer instruction set
* M and Zicsr extensions
* Privileged architecture (M mode only)

The following specifications and extensions are stubbed:

* Physical Memory Protection
* A
* Zifencei
* Zicntr
* Zihpm

## Interrupts

Neither CLINT nor PLIC are implemented, but instead a primitive ad-hoc
interruptor with fixed interrupt priorities is provided. Interrupts
are treated as standard traps that set `mcause` to a value with the
highest bit set to 1. The values themselves indicate the kind of
interrupt, and by convention, they're treated as negative signed
numbers.

The following interrupts are currently provided:

```
FrameSync = -1
TouchInput = -2
```

## Memory map

The following memory regions are currently specified (ranges are inclusive):

```
0x01000000 to 0x010176ff: VRAM
0x02000000 to 0x02000005: Touch data
0x03000000 to 0x030003ff: Sound data
0x80000000 to 0x807fffff: SRAM
```

Unspecified regions are reserved and should be treated as nonexistent.

## SRAM

The machine provides 8 MiB of SRAM with its start address (0x80000000)
used by the CPU as the reset vector. The software is free to use it
for both temporary and permanent storage, and should gracefully handle
resuming its state from the SRAM contents after the CPU reset.

## GPU

The built-in display has the following properties:

* Resolution: 240x400 pixels
* Bit depth: 8 bits
* Palette: 256 shades of gray
* Refresh rate: 30 Hz

Its contents are mapped in VRAM as a linear framebuffer and can be
written in by the software. In order to synchronize rendering to the
refresh rate, the software should rely on the `FrameSync` interrupt
which is fired right after a frame has been displayed, indicating that
it's a good time to render another.

## Touch input

The built-in touchscreen is the only provided way for user input. When
an interaction occurs, a `TouchInput` interrupt is fired and the
information about it is written to the touch data memory region. The
software shouldn't attempt to write in this region as it's read-only.

The data is represented as 3 unsigned 16 bit integers at the following offsets:

```
0 = Type of interaction
1 = Absolute X offset in display pixels
2 = Absolute Y offset in display pixels
```

The following types of interactions are currently defined:

```
Down = 0
Move = 1
Up = 2
```

## Sound output

The built-in speaker has the following properties:

* Sample rate: 30720 Hz (1024 samples per video frame)
* Format: signed integer PCM
* Bit depth: 8 bits

In order to play audio, the software should write samples to the sound
data memory region which contains enough space to hold samples for one
video frame. Since the samples should be updated on each frame, the
`FrameSync` interrupt must be used to time the updates.
