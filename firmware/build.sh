#!/bin/sh

set -uex

FILENAME=firmware.bm1

riscv64-unknown-elf-gcc\
    -march=rv32im_zicsr -mabi=ilp32 -mcmodel=medany\
    -ffreestanding -nostdlib -nostdinc\
    -Wall -Wextra -Wpedantic -Wno-unused-variable\
    -Os -std=c99\
    -ffunction-sections -Wl,-T link.ld\
    -I assets src/*.S src/*.c

riscv64-unknown-elf-objcopy -O binary a.out "${FILENAME}"

mkdir -p ../public
mv "${FILENAME}" ../public
