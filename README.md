BlastMachine One is a WIP fantasy PDA from the '90s.

## Specs

* CPU: RISC-V 32IM core at 16 MHz
* Memory: 8 MiB of SRAM
* Display: 240x400 pixels, 8 bit monochrome at 30 Hz
* Input: single-touch screen
* Audio: 8 bit, 30 kHz PCM playback

For more information, see the [hardware reference].

[hardware reference]: HACKING.md

## Build dependencies

* Rust
* Cargo
* [wasm-pack]
* npm
* POSIX environment (optional, for the ISA tests and the firmware)

[wasm-pack]: https://rustwasm.github.io/wasm-pack/

## How to build

```sh
npm install
npm run build
```

## How to debug

```sh
npm run start
```

## How to test the CPU instruction set

1. Make sure you have the [RISC-V GNU toolchain] installed. If you
   don't, either build it manually or get it from your distro.
2. Make sure the toolchain's `bin` directory is in your `$PATH`.
3. Run the following:

[RISC-V GNU toolchain]: https://github.com/riscv-collab/riscv-gnu-toolchain

```sh
cd test
./build.sh
./run.sh
```

## How to build the firmware

1. Make sure you have the toolchain mentioned in the section above
   installed.
2. Run the following:

```sh
cd firmware
./build.sh
```

## How to publish

1. Test it locally first.
2. Configure your static website host to detect changes in the
   `production` branch and ignore the `default` branch.
3. Select `dist` as the output directory.
4. Run the following:

```sh
./publish.sh
git push --all
```

## Inspiration

Hardware:

* Game Boy Advance
* HP Jornada 620LX
* Macintosh 128K
* Nokia 9000 Communicator

Implementation:

* [Computerraria](https://github.com/misprit7/computerraria)
* [JSNES](https://github.com/bfirsh/jsnes)
* [mini-rv32ima](https://github.com/cnlohr/mini-rv32ima)
* [rv](https://github.com/mnurzia/rv)

Naming:

* Blast Processing
* Essential PH-1

## License

Copyright 2023 Warning: Non-Potable Water

[GNU AGPL v3 or later](LICENSE).
