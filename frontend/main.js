import init, {set_panic_hook, Machine} from "bm1";

const FRAMES_PER_SECOND = 30;
const SAMPLES_PER_SECOND = 30720;

const WIDTH = 240;
const HEIGHT = 400;

const MS_PER_FRAME = 1000 / FRAMES_PER_SECOND;
const SAMPLES_PER_FRAME = SAMPLES_PER_SECOND / FRAMES_PER_SECOND;

const FRAMEBUFFER_SIZE = WIDTH * HEIGHT * 4;
const SRAM_SIZE = 8 * 1024 * 1024;

let rom;
let machine;

let canvas;
let canvasContext;
let imageData;

let audioContext;
let soundData;

let lastFrame = performance.now();
let hadError = false;
let hadTouchDown = false;

main();

async function main() {
    window.addEventListener("error", onError);
    window.addEventListener("unhandledrejection", onError);

    await init();
    set_panic_hook();

    canvas = document.querySelector("#display");
    canvasContext = canvas.getContext("2d");

    canvas.width = WIDTH;
    canvas.height = HEIGHT;

    imageData = canvasContext.getImageData(0, 0, WIDTH, HEIGHT);
    soundData = new Float32Array(SAMPLES_PER_FRAME);

    attachInput();
    attachSound();

    await loadFirmware();

    render();
}

function attachInput() {
    const resetButton = document.querySelector("#reset");
    const loadButton = document.querySelector("#load");
    const saveButton = document.querySelector("#save");

    resetButton.addEventListener("click", onReset);
    saveButton.addEventListener("click", onSave);
    loadButton.addEventListener("click", onLoad);

    window.addEventListener("resize", onResize);

    canvas.addEventListener("mousedown", onTouchDown);
    canvas.addEventListener("touchstart", onTouchDown);

    canvas.addEventListener("mousemove", onTouchMove);
    canvas.addEventListener("touchmove", onTouchMove);

    canvas.addEventListener("mouseup", onTouchUp);
    canvas.addEventListener("touchend", onTouchUp);

    canvas.addEventListener("mouseleave", onTouchUp);
    canvas.addEventListener("touchcancel", onTouchUp);
}

function attachSound() {
    audioContext = new AudioContext({
        numberOfChannels: 1,
        sampleRate: SAMPLES_PER_SECOND,
    });

    // Yes, it's deprecated, but it's the only thing that actually works.

    const scriptProcessor = audioContext.createScriptProcessor(
        SAMPLES_PER_FRAME, 0, 1
    );

    scriptProcessor.addEventListener("audioprocess", event => {
        if (hadError) {
            return;
        }

        event.outputBuffer.copyToChannel(soundData, 0);
    });

    scriptProcessor.connect(audioContext.destination);
}

async function loadFirmware() {
    const response = await fetch("/firmware.bm1");

    if (!response.ok) {
        throw new Error("HTTP server couldn't deliver the firmware.");
    }

    const buffer = await response.arrayBuffer();

    if (buffer.byteLength > SRAM_SIZE) {
        throw new RangeError("The firmware is too big to be loaded.");
    }

    rom = new Uint8Array(buffer);

    onReset();
}

async function loadRom(blob) {
    if (blob.size > SRAM_SIZE) {
        throw new RangeError("The rom is too big to be loaded.");
    }

    const buffer = await blob.arrayBuffer();

    rom = new Uint8Array(buffer);

    onReset();
}

function render() {
    if (hadError) {
        return;
    }

    // Idea stolen from https://chriscourses.com/blog/standardize-your-javascript-games-framerate-for-different-monitors

    requestAnimationFrame(render);

    const currentFrame = performance.now();
    const timePassed = currentFrame - lastFrame;

    if (timePassed < MS_PER_FRAME) {
        return;
    }

    const timeExceeded = currentFrame % MS_PER_FRAME;
    lastFrame = currentFrame - timeExceeded;

    machine.frame(imageData.data, soundData);

    canvasContext.putImageData(imageData, 0, 0);
}

function onError(event) {
    const error = event.error ?? event.reason ?? event;

    console.error(error);

    if (hadError) {
        return;
    }
    hadError = true;

    const container = document.querySelector("#error");
    const message = document.querySelector("#message");

    container.hidden = false;

    message.innerText = `${error.name}: ${error.message}`;

    if ("stack" in error) {
        message.innerText += `\n\n${error.stack}`;
    }
}

function onReset() {
    machine = new Machine(rom);

    onResize();
}

function onLoad() {
    const picker = document.createElement("input");

    picker.type = "file";
    picker.accept = ".bm1";

    picker.addEventListener("change", event => {
        const files = event.target.files;

        if (files.length == 0) {
            return;
        }

        const file = files[0];

        loadRom(file);
    });

    picker.click();
}

function onSave() {
    const sram = new Uint8Array(SRAM_SIZE);

    machine.dump(sram);

    const blob = new Blob([sram]);
    const url = URL.createObjectURL(blob);

    const link = document.createElement("a");

    link.href = url;
    link.download = `sram-dump-${Date.now()}.bm1`;

    link.click();

    URL.revokeObjectURL(url);
}

function onResize() {
    const viewport = canvas.parentNode;

    const viewportWidth = viewport.clientWidth * devicePixelRatio;
    const viewportHeight = viewport.clientHeight * devicePixelRatio;

    const viewportAspectRatio = viewportWidth / viewportHeight;
    const canvasAspectRatio = WIDTH / HEIGHT;

    const absoluteScale = viewportAspectRatio > canvasAspectRatio
        ? viewportHeight / HEIGHT
        : viewportWidth / WIDTH;

    // The scale factor should preferably be an integer, but on some small
    // screens it just doesn't work.

    const scale = absoluteScale > 2
        ? Math.floor(absoluteScale) / devicePixelRatio
        : absoluteScale / devicePixelRatio;

    const targetWidth = WIDTH * scale;
    const targetHeight = HEIGHT * scale;

    canvas.style.width = `${targetWidth}px`;
    canvas.style.height = `${targetHeight}px`;
}

function onTouchDown(event) {
    hadTouchDown = true;

    const [x, y] = eventToCoordinates(event);

    machine.enqueue_touch_down(x, y);

    // Has to be done due to the autoplay prevention.

    audioContext.resume();
}

function onTouchMove(event) {
    if (!hadTouchDown) {
        return;
    }

    const [x, y] = eventToCoordinates(event);

    machine.enqueue_touch_move(x, y);
}

function onTouchUp(event) {
    if (!hadTouchDown) {
        return;
    }
    hadTouchDown = false;

    const [x, y] = eventToCoordinates(event);

    machine.enqueue_touch_up(x, y);
}

function eventToCoordinates(e) {
    if (e.cancelable) {
        e.preventDefault();
    }

    const event = "changedTouches" in e ? e.changedTouches[0] : e;

    const [viewportX, viewportY] = [event.clientX, event.clientY];
    const [canvasX, canvasY] =
        getCanvasCoordinates(event.clientX, event.clientY);

    return [canvasX, canvasY];
}

function getCanvasCoordinates(clientX, clientY) {
    const rect = canvas.getBoundingClientRect();

    const x = clientX - rect.left;
    const y = clientY - rect.top;

    let localX = Math.round(x / rect.width * canvas.width);
    let localY = Math.round(y / rect.height * canvas.height);

    if (localX < 0) {
        localX = 0;
    } else if (localX >= canvas.width) {
        localX = canvas.width - 1;
    }

    if (localY < 0) {
        localY = 0;
    } else if (localY >= canvas.height) {
        localY = canvas.height - 1;
    }

    return [localX, localY];
}
