#!/bin/sh

set -uex

git submodule update --init --recursive

cd riscv-tests

autoconf
./configure --prefix="$(realpath '../target')"

make
make install

cd ..

ELF_DIR='target/share/riscv-tests/isa'
BIN_DIR='target/cases'

CASES="$(find "${ELF_DIR}" -name 'rv32[um][iam]-p-*' -not -name '*.dump')"

mkdir -p "${BIN_DIR}"

for CASE in $CASES
do
    riscv64-unknown-elf-objcopy -O binary\
        "${CASE}" "${BIN_DIR}"/$(basename "${CASE}")
done

# Misaligned data accesses are optional, so we don't check for them. See Chapter
# 2.6 of the unprivileged spec.

rm "${BIN_DIR}"/rv32ui-p-ma_data
