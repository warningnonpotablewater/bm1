#!/bin/sh

set -ue

RUNNER=../target/debug/isa-test

RED='\033[0;31m'
GREEN='\033[0;32m'
NORMAL='\033[0m'

cd ..
cargo build
cd test

for CASE in target/cases/*
do
    if "${RUNNER}" "${CASE}"
    then
        echo -e "${GREEN}$(basename "${CASE}"): PASS${NORMAL}"
    else
        echo -e "${RED}$(basename "${CASE}"): FAIL${NORMAL}"
    fi
done
